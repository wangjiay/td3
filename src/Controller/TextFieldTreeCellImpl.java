package Controller;

import Model.Group;
import Model.Model;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class TextFieldTreeCellImpl extends TreeCell<Object> {

    private TextField textField;
    static TreeView<Object> tree;
    private Model model;

    public TextFieldTreeCellImpl(TreeView<Object> tree, Model model) {
        this.tree = tree;
        this.model = model;
    }

    @Override
    public void startEdit() {

        if(!(model.getGroups().get(model.getGroupIndex(getTreeItem().getValue().toString())) instanceof Group)) {
            return;
        }
        super.startEdit();

        if (textField == null) {
            createTextField();
        }

        setText(null);
        setGraphic(textField);
        textField.selectAll();

    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText((String) getItem());
        setGraphic(getTreeItem().getGraphic());
    }

    @Override
    public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(getTreeItem().getGraphic());
            }
        }
    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent t) {

                if (t.getCode() == KeyCode.ENTER) {
                    String newName = textField.getText();
                    String oldName = getTreeItem().getValue().toString();
                    model.getGroups().get(model.getGroupIndex(oldName)).setName(newName);
                    commitEdit(newName);
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    static Integer getTreeItemIndex (TreeItem<Object> root, String name) {
        Integer i = 0;
        while(!root.getChildren().get(i).getValue().toString().equals(name)) {
            i++;
        }

        return i;
    }
}

