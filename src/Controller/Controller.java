package Controller;

import Model.Contact;
import Model.Country;
import Model.Model;
import Model.Group;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;

import java.util.HashMap;

public class Controller {

    private Contact contact;
    private Model model;

    @FXML
    private TextField firstName = new TextField();
    @FXML
    private TextField lastName = new TextField();
    @FXML
    private TextField addressName = new TextField();
    @FXML
    private TextField postcode = new TextField();
    @FXML
    private TextField city = new TextField();
    @FXML
    private ComboBox<String> countryComboBox;
    @FXML
    private DatePicker birthDate = new DatePicker();
    @FXML
    private ToggleGroup sex;
    @FXML
    private RadioButton male = new RadioButton();
    @FXML
    private RadioButton female = new RadioButton();
    @FXML
    private Button validate;
    @FXML
    private VBox contactPane;

    private MapChangeListener<String,String> listener;

    private HashMap<String, Control> controlsMap;

    private ListChangeListener<Group> groupsListener;

    // groups
    @FXML
    private TreeView<Object> tree;
    @FXML
    private Button add = new Button();
    @FXML
    private Button remove = new Button();

    public Controller () {
        this.contact = new Contact();
        this.model = new Model();
        this.controlsMap = new HashMap<String, Control>();
        this.tree = new TreeView<Object>();
    }

    @FXML
    public void initialize(){

        // Hide contactPane
        contactPane.visibleProperty().set(false);

        // Initialize countries
        countryComboBox.setItems(Country.getCountries());

        // Binding view -> model
        this.firstName.textProperty().bindBidirectional(this.contact.firstNameProperty());
        this.lastName.textProperty().bindBidirectional(this.contact.lastNameProperty());
        this.addressName.textProperty().bindBidirectional(this.contact.getAddress().addressNameProperty());
        this.postcode.textProperty().bindBidirectional(this.contact.getAddress().postcodeProperty(), new NumberStringConverter());
        this.city.textProperty().bindBidirectional(this.contact.getAddress().cityProperty());

        this.countryComboBox.getSelectionModel().selectedItemProperty().addListener((obs, oldV, newV) -> {
            this.contact.getAddress().setCountry(newV);
        });

        this.sex.selectedToggleProperty().addListener((obs, oldV, newV) -> {
            RadioButton radio = (RadioButton) sex.getSelectedToggle();
            this.contact.setSex(radio.getText());
        });

        this.birthDate.valueProperty().addListener((obs, oldV, newV) -> {
            this.contact.setBirthDate(newV);
        });

        // validation contact
        this.controlsMap.put(this.contact.firstNameProperty().getName(), this.firstName);
        this.controlsMap.put(this.contact.lastNameProperty().getName(), this.lastName);
        this.controlsMap.put(this.contact.getAddress().addressNameProperty().getName(), this.addressName);
        this.controlsMap.put(this.contact.getAddress().postcodeProperty().getName(), this.postcode);
        this.controlsMap.put(this.contact.getAddress().cityProperty().getName(), this.city);
        this.controlsMap.put(this.contact.getAddress().countryProperty().getName(), this.countryComboBox);
        this.controlsMap.put(this.contact.birthDateProperty().getName(), this.birthDate);

        listener = changed -> {
            if(changed.wasAdded()) {
                if(changed.getKey() == this.contact.sexProperty().getName()) {
                    this.male.setStyle("-fx-border-color: red ;");
                    this.female.setStyle("-fx-border-color: red ;");
                    this.male.setTooltip(new Tooltip(changed.getValueAdded()));
                    this.female.setTooltip(new Tooltip(changed.getValueAdded()));
                } else {
                    this.controlsMap.get(changed.getKey()).setStyle("-fx-border-color: red ;");
                    this.controlsMap.get(changed.getKey()).setTooltip(new Tooltip(changed.getValueAdded()));
                }
            } else {
                if(changed.getKey() == this.contact.sexProperty().getName()) {
                    this.male.setStyle("-fx-border-color: null ;");
                    this.female.setStyle("-fx-border-color: null ;");
                    this.male.setTooltip(null);
                    this.female.setTooltip(null);
                } else{
                this.controlsMap.get(changed.getKey()).setStyle("-fx-border-color: null ;");
                this.controlsMap.get(changed.getKey()).setTooltip(null);
                }
            }
        };

        this.contact.getErrors().addListener(listener);

        this.validate.setOnAction(evt -> {

            // Validate fields
            this.contact.validate();

            // Check if selected node is a group
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
            Object selectedItemValue = model.getGroups().get(model.getGroupIndex(selectedItem.getValue().toString()));
            if (selectedItemValue instanceof Group) {

                // Create new contact
                try {
                    Contact newContact = (Contact) contact.clone();
                    ((Group) selectedItemValue).getContacts().add(newContact);

                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }

        });

        // Handle groups
        TreeItem<Object> rootNode;
        rootNode = new TreeItem<Object>("Fiche de contacts");
        rootNode.setExpanded(true);
        tree.setRoot(rootNode);
        tree.setEditable(true);
        tree.setCellFactory(param -> new TextFieldTreeCellImpl(tree, model));

        // Contacts listener
        ListChangeListener<Contact> contactsListener = change -> {
            TreeItem<Object> parentGroup = tree.getSelectionModel().getSelectedItem();
            change.next();
            if (change.wasAdded()) {
                change.getAddedSubList().forEach(c -> {
                    TreeItem<Object> newContact = new TreeItem(c.getFirstName() + " " + c.getLastName(), new ImageView((new Image("Images/contact.png"))));
                    parentGroup.getChildren().add(newContact);
                });
            }

            if (change.wasRemoved()) {
                System.out.println("hello");
                change.getRemoved().forEach(c -> {
                    Integer index = TextFieldTreeCellImpl.getTreeItemIndex(parentGroup,c.getFirstName() + c.getLastName());
                    TreeItem toRemove = parentGroup.getChildren().get(index);
                    parentGroup.getChildren().remove(toRemove);
                });
            }
        };

        // Add group or contact
        add.setOnAction(evt -> {
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();

            // Add new group
            if (selectedItem.getValue() == rootNode.getValue()) {

                // Create group
                Group group = new Group();
                model.getGroups().add(group);

                // Bind listener
                group.getContacts().addListener(contactsListener);
            }

            // Fill info of new contact
            else if (model.getGroups().get(model.getGroupIndex(selectedItem.getValue().toString())) instanceof Group) {
                contactPane.visibleProperty().set(true);
            }

        });

        // Remove group or contact
        remove.setOnAction(evt -> {
            TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
            String[] split = selectedItem.getValue().toString().split(" ");

            // Remove group
            if (split.length == 1) {
                model.getGroups().remove(model.getGroups().get(model.getGroupIndex(selectedItem.getValue().toString())));
            }

            else {
                Object parentValue = model.getGroups().get(model.getGroupIndex(selectedItem.getParent().getValue().toString()));
                if (parentValue instanceof Group) {
                    String firstName = split[0];
                    String lastName = split[1];
                    ((Group) parentValue).getContacts().remove(((Group) parentValue).getContactIndex(firstName, lastName));
                }

            }
        });

        // groupsListener
        groupsListener = change -> {
            change.next();
            if(change.wasAdded()) {
                change.getAddedSubList().forEach(g -> {
                    TreeItem<Object> newGroup = new TreeItem(g.getName(), new ImageView(new Image("Images/group.png")));
                    newGroup.setExpanded(true);
                    rootNode.getChildren().add(newGroup);
                });
            }

            if (change.wasRemoved()) {
                change.getRemoved().forEach(g -> {
                    Integer index = TextFieldTreeCellImpl.getTreeItemIndex(rootNode, g.getName());
                    TreeItem toRemove = rootNode.getChildren().get(index);
                    rootNode.getChildren().remove(toRemove);
                });
            }
        };

        this.model.getGroups().addListener(groupsListener);

    }
}