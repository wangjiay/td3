package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Address {

    private StringProperty addressName;
    private IntegerProperty postcode;
    private StringProperty city;
    private StringProperty country;

    public String getAddressName() {
        return addressName.get();
    }

    public StringProperty addressNameProperty() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName.set(addressName);
    }

    public int getPostcode() {
        return postcode.get();
    }

    public IntegerProperty postcodeProperty() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode.set(postcode);
    }

    public String getCity() {
        return city.get();
    }

    public StringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getCountry() {
        return country.get();
    }

    public StringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }

    public Address() {
        this.addressName = new SimpleStringProperty(this, "addressName", null);
        this.postcode = new SimpleIntegerProperty(this, "postcode, null");
        this.city = new SimpleStringProperty(this, "city", null);
        this.country = new SimpleStringProperty(this, "country", null);
    }
}
