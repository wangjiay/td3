package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {
    private ObservableList<Group> groups;

    public ObservableList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ObservableList<Group> groups) {
        this.groups = groups;
    }

    public Model() {
        this.groups = FXCollections.observableArrayList();
    }

    public Integer getGroupIndex (String groupName) {
        Integer i = 0;
        while (!this.getGroups().get(i).getName().equals(groupName) && i < this.getGroups().size()) {
            i++;
        }

        if (i < this.getGroups().size()) {
            return i;
        }

        System.out.print(i);
        return -1;
    }
}
