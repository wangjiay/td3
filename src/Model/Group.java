package Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Group {
    private StringProperty name;
    private ObservableList<Contact> contacts;

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public ObservableList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ObservableList<Contact> contacts) {
        this.contacts = contacts;
    }

    public Group() {
        this.contacts = FXCollections.observableArrayList();
        this.name = new SimpleStringProperty(null, "name", "My new group");
    }

    public Integer getContactIndex (String firstName, String lastName) {
        Integer i = 0;
        while (!(contacts.get(i).getLastName().equals(lastName) && this.getContacts().get(i).getFirstName().equals(firstName))) {
            i++;
        }
        return i;
    }
}
